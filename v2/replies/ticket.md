Структуры возвращаемые в запросах
=====================================

Ticket
-------------

Структура для описания информации о билете.

|  Параметр  	|              Описание             	|
|:----------:	|:---------------------------------:	|
|  ticketId  	|      идентификатор в системе      	|
|   placeId  	|        идентификатор места        	|
|    price   	|                цена               	|
| uniqueCode 	| уникальный код для печати QR-кода 	|

* Далее: [Структуры возвращаемые в запросах. TicketCheck](ticketCheck)
* Ранее: [Структуры возвращаемые в запросах. Ticket](ticket)
* [Содержание](../index)